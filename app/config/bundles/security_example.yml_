security:
    acl:
        connection: default

    access_decision_manager:
        # authorization
        # strategy can be: affirmative, unanimous or consensus
        # affirmative - jakmile jeden voter vrátí (GRANTED), je přístup umožněn
        # unanimous - žádný voter nesmí vrátit (DENIED)
        strategy: unanimous
        allow_if_all_abstain:  false
        allow_if_equal_granted_denied:  true


    encoders:
        Symfony\Component\Security\Core\User\User: plaintext
#        Webuni\Bundle\SecurityBundle\User\WebuniUser: plaintext
        Webuni\Bundle\SecurityBundle\User\WebuniUser:
            algorithm: sha512
            encode_as_base64: false
            iterations: 1
        FOS\UserBundle\Model\UserInterface:
#            id: webuni_security.custom_encoder
            algorithm: md5
            encode_as_base64: false
            iterations: 1

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_EDITOR:      ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]


    # definice služeb, které dokážou vyhledat uživatele
    providers:
        # provider, který zřetězí jiné providery =>
        # pokud se uživatel nenajde pomocí prvního zmíněného,
        # zkusí se druhý v pořadíí atd.
        chain_provider:
            chain:
                providers: [in_memory, webuni_user_provider]

        # jednoduchý provider umožňující zadat uživatele v konfiguračním
        # souboru
        in_memory:
            memory:
                users:
                    user:  { password: userpass, roles: [ 'ROLE_USER' ] }
                    admin: { password: adminpass, roles: [ 'ROLE_ADMIN' ] }

        # provider pro načítání uživatelů z databáze
#        user_db:
#            entity: { class: Acme\UserBundle\Entity\User, property: username }
#

        # vlastní služba, která musí implementovat
        # Symfony\Component\Security\Core\User\UserProviderInterface
        webuni_user_provider:
            id: webuni_security.user_provider

        # provider z FosUserBundle
        fos_userbundle:
            id: fos_user.user_provider.username


    # Firewall je vstupní bod pro spuštění kontroly přístupu
    # V aplikaci si můžeme nastavit neomezené množství firewallů pro
    # různé části aplikace.
    # Firewally mezi sebou nesdílí context
    firewalls:
        dev:
            pattern:  ^/(_(profiler|wdt)|css|images|js)/
            security: false

#        user_creation:
#            pattern:  ^/user/create$
#            security: false

        login:
            pattern:  ^/login$
            security: false


        secured_area:
            # pravidla určující, kdy bude firewall zapnutý
            # bez zapnutého firewallu není vytvořený security token
            # není tedy možné používat metodu isGranted
            pattern:  ^/
#            methods: [GET, POST]
#            host: ^localhost
#            host: ^admin\.webuni\.cz

            # pokud není provider explicitně specifikovaný, použije se
            # první v pořadí ze sekce providers výše
            provider: chain_provider

            # nastavení, které říká, že na všech url adresách hlídaných
            # tímto firewallem (secured_area) se automaticky vytvoří
            # anonymous token, který odpovídá roli IS_AUTHENTICATED_ANONYMOUSLY
#            anonymous: ~        # pokud povolime annonymous, nemusíme mít zvlášt 'login' firewall

            # Nastavení pro přihlašování pomocí formuláře
            form_login:
#                check_path: login_check
#                login_path: login
                default_target_path: webuni_admin.dashboard.index
#                username_parameter: webuni_login_form[username]
#                password_parameter: webuni_login_form[password]
#                success_handler: webuni_security.authentication.success_handler
#                failure_handler: webuni_security.authentication.failure_handler
            logout:
                target: login
            #http_basic:
            #    realm: "Secured Demo Area"


    # Řízení přístupu.
    # Pravidla se vyhodnocují postupně od prvního k poslednímu podle následujících hodnot:
    # URI, IP, HOST a METHOD
    #
    # Jakmile se najde pravidlo, které se schoduje s requestem podle těch 4 hodnot,
    # začne se řešit, jestli také sedí hodnoty
    # ROLE, CHANNEL, ALLOW_IF
    access_control:
        - { path: ^/admin, roles: ROLE_ADMIN }
#        - { path: ^/admin, roles: ROLE_USER }   # nikdy se nepoužije, protože vždy bude dříve vyhovovat
#                                                # předchozí pravidlo (URI = ^/admin)
        - { path: ^/admin, roles: ROLE_USER_IP, ip: 127.0.0.1 }
        - { path: ^/admin, roles: ROLE_USER_HOST, host: symfony\.com$ }
        - { path: ^/admin, roles: ROLE_USER_METHOD, methods: [POST, PUT] }
#        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY, requires_channel: https }
#        -
#            path: ^/_internal/secure
#            allow_if: "'127.0.0.1' == request.getClientIp() or has_role('ROLE_ADMIN')" # od verze 2.4

jms_security_extra:
    secure_all_services: false
