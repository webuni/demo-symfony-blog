<?php

$files = \Symfony\Component\Finder\Finder::create()
    ->name('/\.(yml|xml)$/')
    ->in(__DIR__.'/bundles')
;

foreach ($files as $file) {
    $loader->import((string) $file);
}
