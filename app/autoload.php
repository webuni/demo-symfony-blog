<?php

/*
 * This file is part of the Symfony Minimal Edition package.
 *
 * (c) Webuni s.r.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

if (!$loader = include __DIR__.'/../vendor/autoload.php') {
    $nl = PHP_SAPI === 'cli' ? PHP_EOL : '<br />';
    echo "$nl$nl";
    die('You must set up the project dependencies.'.$nl.
        'Run the following commands in '.dirname(__DIR__).':'.$nl.$nl.
        'curl -s http://getcomposer.org/installer | php'.$nl.
        'php composer.phar install'.$nl);
}

use Doctrine\Common\Annotations\AnnotationRegistry;

// intl
if (!function_exists('intl_get_error_code')) {
    if (is_file($file = __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/functions.php')) {
        require_once $file;
    }

    if (is_file($file = __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs')) {
        $loader->add('', $file);
    }
}

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
