<?php

/*
 * This file is part of the Symfony Minimal Edition package.
 *
 * (c) Webuni s.r.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle implements CompilerPassInterface
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass($this);
    }

    public function process(ContainerBuilder $container)
    {
        //$this->processTwig($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function processTwig(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('twig');
        foreach ($container->findTaggedServiceIds('twig.extension') as $id => $attributes) {
            $definition->addMethodCall('addExtension', [new Reference($id)]);
        }
    }

    protected function processSecurity(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('security.access.decision_manager')) {
            return;
        }

        $voters = new \SplPriorityQueue();
        foreach ($container->findTaggedServiceIds('security.voter') as $id => $attributes) {
            $priority = isset($attributes[0]['priority']) ? $attributes[0]['priority'] : 0;
            $voters->insert(new Reference($id), $priority);
        }

        $voters = iterator_to_array($voters);
        ksort($voters);

        $container->getDefinition('security.access.decision_manager')->replaceArgument(0, array_values($voters));
    }
}
