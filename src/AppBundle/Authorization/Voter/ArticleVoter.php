<?php

namespace AppBundle\Authorization\Voter;

use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class ArticleVoter implements VoterInterface
{
    private $doctrine;
    private $requestStack;
    private $blacklistedIp = [];
    private $allowedAttributes = [
        'SHOW_ARTICLE', 'SHOW_OWN_ARTICLE', 'EDIT_ARTICLE', 'EDIT_OWN_ARTICLE',
    ];

    public function __construct(RequestStack $requestStack, Registry $doctrine, array $blacklistedIp = [])
    {
        $this->doctrine = $doctrine;
        $this->requestStack = $requestStack;
        $this->blacklistedIp = $blacklistedIp;
    }

    public function supportsAttribute($attribute)
    {
        return in_array($attribute, $this->allowedAttributes);
    }

    public function supportsClass($class)
    {
        // your voter supports all type of token classes, so return true
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        /** @var User $user */
        /* @var Article $article */
        $user = $token->getUser();
        $article = $object;

        // check each attribute one by one
        foreach ($attributes as $attribute) {
            if (!$this->supportsAttribute($attribute) || !$this->supportsClass($object)) {
                continue;
            }

            if ('SHOW_ARTICLE' === $attribute) {
                return VoterInterface::ACCESS_GRANTED;
            }

            if ('SHOW_OWN_ARTICLE' === $attribute) {
                //                if ($user->hasRole('ROLE_ADMIN')){
//                    return VoterInterface::ACCESS_GRANTED;
//                }

                $article = $this->doctrine->getRepository('WebuniAdminBundle:Article')->findOneBy([
                    'id' => $article->getId(),
                    'user' => $user,
                ]);

                if ($article instanceof Article) {
                    return VoterInterface::ACCESS_GRANTED;
                }
            }
        }

        return VoterInterface::ACCESS_DENIED;
    }
}
