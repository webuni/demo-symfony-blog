<?php

namespace AppBundle\Menu;

use AppBundle\Event\MenuCreatedEvent;
use AppBundle\Event\MenuEvents;
use Knp\Menu\FactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    private $factory;
    private $eventDispatcher;

    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher)
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createMainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes([
            'class' => 'nav navbar-nav navbar-right',
        ]);

        $this->eventDispatcher->dispatch(
            MenuEvents::CREATED,
            new MenuCreatedEvent($this->factory, $menu)
        );

        return $menu;
    }
}
