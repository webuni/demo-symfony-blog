<?php

namespace AppBundle\EventListener;

use AppBundle\Event\MenuCreatedEvent;
use AppBundle\Event\MenuEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminMenuListener implements EventSubscriberInterface
{
    public function onMainMenuCreated(MenuCreatedEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('admin_post_list', [
            'route' => 'admin_post_index',
            'label' => '<i class="fa fa-list-alt"></i> Post List',
            'extras' => ['safe_label' => true],
        ]);

        $menu->addChild('admin_blog_index', [
            'route' => 'blog_index',
            'label' => '<i class="fa fa-home"></i> Back to blog',
            'extras' => ['safe_label' => true],
        ]);
    }

    public static function getSubscribedEvents()
    {
        return [
            MenuEvents::CREATED => ['onMainMenuCreated', 10],
        ];
    }
}
