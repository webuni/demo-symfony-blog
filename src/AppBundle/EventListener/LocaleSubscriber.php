<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        // overime si, jestli neni nastaven query requestu _locale
        $request = $event->getRequest();
        if ($locale = $request->query->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // jestlize uzivatel v url explicitne nenastavil _local, pouzijeme ze sessiony
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => ['onRequest', 12],
        ];
    }
}
