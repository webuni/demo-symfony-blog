<?php

namespace AppBundle\EventListener;

use AppBundle\Event\MenuCreatedEvent;
use AppBundle\Event\MenuEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ExampleMenuListener implements EventSubscriberInterface
{
    public function onMainMenuCreated(MenuCreatedEvent $event)
    {
        $menu = $event->getMenu();

        // root Example item
        $exItem = $menu->addChild('example', [
            'route' => 'homepage',
            'label' => 'Examples',
        ]);

        $exItem->setAttributes([
            'class' => 'dropdown',
        ]);
        $exItem->setLinkAttributes([
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle',
        ]);
        $exItem->setChildrenAttributes([
            'class' => 'dropdown-menu',
        ]);

        // Controller item
        $exItem->addChild('Controller', [
            'route' => 'app_controller_index',
        ]);

        // Translation item
        $exItem->addChild('Translation', [
            'route' => 'app_translation_index',
        ]);

        // Twig item
        $exItem->addChild('Twig', [
            'route' => 'app_twig_index',
        ]);
    }

    public static function getSubscribedEvents()
    {
        return [
            MenuEvents::CREATED => ['onMainMenuCreated', 0],
        ];
    }
}
