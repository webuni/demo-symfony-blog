<?php

namespace AppBundle\EventListener;

use AppBundle\Event\MenuCreatedEvent;
use AppBundle\Event\MenuEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BlogMenuListener implements EventSubscriberInterface
{
    public function onMainMenuCreated(MenuCreatedEvent $event)
    {
        $menu = $event->getMenu();

        $homeItem = $menu->addChild('Homepage', [
            'route' => 'homepage',
            'label' => '<i class="fa fa-home"></i>Homepage',
            'extras' => ['safe_label' => true],
        ]);

        $homeItem->setAttributes([
            'class' => 'navbar-item',
        ]);

        $homeItem->setLinkAttributes([
            'class' => 'link-class',
        ]);
    }

    public static function getSubscribedEvents()
    {
        return [
            MenuEvents::CREATED => ['onMainMenuCreated', 20],
        ];
    }
}
