<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/trans")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/{_locale}/{count}", defaults={"_locale": "en"})
     * @Template("translation/index.html.twig")
     *
     * @see http://symfony.com/doc/current/book/translation.html
     * @see http://symfony.com/doc/current/components/translation/usage.html
     */
    public function indexAction($count = 1)
    {
        // TODO
        // ukazat configuraci ve framework.yml
        // ukazat preklady v kontroleru a sablone
        // ukazat LocaleListener
        // ukazat _locale v url
        // vicejazycne routy
        //      @see https://github.com/schmittjoh/JMSI18nRoutingBundle
        // preklad databaze
        //      @see https://github.com/l3pp4rd/DoctrineExtensions
        //      @see https://github.com/KnpLabs/DoctrineBehaviors (pro PHP 5.4+)

        $name = 'Petr';
        $translator = $this->get('translator');

        return [
            'name' => $name,
            'count' => $count,
            'text' => $translator->trans(
                'example.text'
            ),
            'placeholder' => $translator->trans(
                'example.placeholder', ['%name%' => $name]
            ),
            'plural_implicit' => $translator->transChoice(
                'example.plural_implicit', $count, ['%count%' => $count]
            ),
            'plural_explicit' => $translator->transChoice(
                'example.plural_explicit', $count, ['%count%' => $count]
            ),
        ];
    }
}
