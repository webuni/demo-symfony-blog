<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @Route("/forms")
 */
class FormsController extends Controller
{
    /**
     * @Route("/inline", name="forms.inline")
     * @Template("forms/index.html.twig")
     */
    public function inlineAction(Request $request)
    {
        if ($request->query->get('notice') == 1) {
            $this->addFlash('notice', 'Pozor na neco');
        }

        $defaultData = [
            'title' => 'Petr',
            'content' => 'content',
            'publishedAt' => new \DateTime(),
        ];

        $form = $this->createFormBuilder($defaultData, [
            'action' => $this->generateUrl('forms.inline'),
            'validation_groups' => false,
            // 'method' => 'PUT'
        ])
            ->add('title', TextType::class, [
                'label' => 'blog.column.title',
                'label_attr' => [
                    'class' => 'special-label',
                    'data-role' => 'label',
                ],
                'required' => false,
                'trim' => true,
                'disabled' => false,
                'error_bubbling' => false,
                'error_mapping' => [
                ],
                'mapped' => true,
                'attr' => [
                    'class' => 'text-field',
                ],
                'constraints' => [
                    // @see http://symfony.com/doc/current/reference/constraints.html
                    new Callback([
                        'callback' => function ($data, ExecutionContextInterface $context) {
                            $context->buildViolation('This title sounds totally fake!')
                                ->atPath('title')
                                ->addViolation();
                        },
                    ]),
                    new NotBlank([
                    ]),
                    new Length([
                        'min' => '3',
                        'minMessage' => 'article.title.min_length',
                    ]),
                ],
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Obsah',
            ])
            ->add('publishedAt', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save it!',
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            dump($form->getData());
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/type", name="forms.type")
     * @Template("forms/index.html.twig")
     */
    public function formTypeFormAction()
    {
        return [
            'form' => $this->createForm(ArticleType::class, new Article())->createView(),
        ];
    }

    /**
     * @Route("/add-article", name="forms.add_article")
     * @Template("forms/index.html.twig")
     */
    public function addArticleAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->add('save', SubmitType::class, [
            'label' => 'Save it!',
        ]);

//        $form->get('publishAt')->getData();
//        $article = $form->getData();

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $article->setSlug($this->get('slugger')->slugify($article->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();

                return $this->redirect($this->generateUrl('forms.add_article'));
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/add-article-clean", name="forms.add-article-clean")
     * @Template("forms/index.html.twig")
     */
    public function cleanFormAction(Request $request)
    {
        $form = $this->createForm(ArticleType::class, new Article());
        $formHandler = $this->get('article_form_handler');

        $form->add('save', SubmitType::class, [
            'label' => 'Save it!',
        ]);

        if ($formHandler->handle($form, $request)) {
            // send confirmation message
            return $this->redirect($this->generateUrl('forms.add_article'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/validation")
     * @Template("forms/index.html.twig")
     */
    public function validationAction()
    {
        $article = new Article();
        $article->setTitle('');
        $article->setContent('d');

        $validator = $this->get('validator');
        $errors = $validator->validate($article);

//        if (count($errors) > 0) {
//            $errorsString = (string) $errors;
//            return new Response($errorsString);
//        }
//        return new Response('Article je validní.');

        return [
            'errors' => $errors,
            'errorCount' => count($errors),
        ];
    }
}
