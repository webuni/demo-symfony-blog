<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/twig")
 */
class TwigController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        // TODO
        // ukazat configuraci framework.yml "framework:templating"
        // ukazat configuraci twig.yml
        // ukazat dědičnost a sestavování šablon:
        //     - extends, include, embed, use, macro, import
        //     - render(controller)
        // ukazat escapovani, kontrola prazdnych znaku
        // ukazat for, if
        // ukazat twig extensions
        // ukazat jak zapnout haml, twital
        // ukazat rozsireni, ktere ukazuje jaka sablona byla pouzita pro vykresleni casti stranky

        return $this->get('templating')->renderResponse('twig/index.html.twig', []);
    }

    public function renderAction()
    {
        return $this->get('templating')->renderResponse('twig/controller.html.twig', [
            'greeting' => 'Toto je text z šablony controller.html.twig',
        ]);
    }
}
