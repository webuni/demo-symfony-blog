<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class LoginController extends Controller
{
    /**
     * @Route("/login")
     * @Template("login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $utils = $this->get('security.authentication_utils');

        $ex = $utils->getLastAuthenticationError();
        if ($ex instanceof BadCredentialsException) {
            $this->addFlash('danger', $ex->getMessage());
        } elseif ($ex instanceof \Exception) {
            $this->addFlash('danger', 'Login failed');
        }

        return [
            'last_username' => $utils->getLastUsername(),
        ];
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
    }

    /**
     * @Route("/logout")
     */
    public function logoutAction()
    {
    }
}
