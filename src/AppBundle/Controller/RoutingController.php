<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/routing")
 */
class RoutingController extends Controller
{
    public function helloAction($name)
    {
        // TODO
        // ukazat front controller
        // ukazat configuraci framework.yml "framework:router"
        // ukazat configuraci routing.yml (definice rout)
        // ukazat jak se generuji url adresy v kontroleru a v sablone
        // ukazat https://github.com/FriendsOfSymfony/FOSJsRoutingBundle

        //dump($request->attributes);

        if ($name == 'Petr') {
            return $this->forward('AppBundle:Routing:hello', ['name' => 'Martin']);
        }

        if ($name == 'Seznam') {
            return $this->redirect('http://seznam.cz');
        }

        if ($name == 'Home') {
            return $this->redirect($this->generateUrl('homepage', [], false));
        }

        if ($name == 'Homepage') {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('routing/hello.html.twig', [
            'name' => $name,
        ]);
    }

    /**
     * @Route("url/{route}")
     */
    public function generateUrlAction(Request $request, $route)
    {
        $parameters = $request->query;

        $this->generateUrl($route, $parameters, UrlGeneratorInterface::ABSOLUTE_PATH);
        $this->generateUrl($route, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
        $this->generateUrl($route, $parameters, UrlGeneratorInterface::NETWORK_PATH);
        $this->generateUrl($route, $parameters, UrlGeneratorInterface::RELATIVE_PATH);

        return $this->render(

        );
    }

    /**
     * @Route("/goodbye/{name}")
     */
    public function goodbyeAction($name)
    {
        return $this->render('routing/goodbye.html.twig', [
            'name' => $name,
        ]);
    }
}
