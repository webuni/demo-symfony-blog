<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/registration")
     * @Template("user/registration.html.twig")
     */
    public function registrationAction(Request $request)
    {
        // vytvoříme registrační formulář
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'method' => 'POST',
        ]);
        $form->add('save', SubmitType::class);

        // zkontrolujeme obdržený request
        // pokud obsahuje data formuláře a je odeslaný nastavenou http metodou
        // tak data vezmeme a naplníme je do objektu $user
        $form->handleRequest($request);

        // validujeme $user objekt a pokud je validní, tak provádíme další operace
        if ($form->isValid()) {
            // encode password
            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($user);
            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

            // persist user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Registration successful');

            // redirection
            return $this->redirect($this->generateUrl('app_user_successregistration'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/registration-success")
     * @Template("user/registered.html.twig");
     */
    public function successRegistrationAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/create")
     * @Template("user/create.html.twig")
     */
    public function createAction(Request $request)
    {
        $name = $request->query->get('name', 'user_'.rand(1, 9999));
        $pass = $request->query->get('pass', 'user');
        $user = new User();

        $encoderFactory = $this->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);

        $user->setUsername($name);
        $user->setFirstname($name);
        $user->setPassword($encoder->encodePassword($pass, $user->getSalt()));
        $user->setRoles(['ROLE_ADMIN']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect('/login');
    }
}
