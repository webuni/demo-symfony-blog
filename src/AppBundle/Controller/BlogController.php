<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/blog")
 */
class BlogController extends Controller
{
    /**
     * @Route("/", name="blog_index")
     * @Method("GET")
     * @Template("blog/index.html.twig")
     */
    public function indexAction()
    {
        //        if (!$this->isGranted("ROLE_ADMIN")) {
//            throw new AccessDeniedException();
//        }

        //$em = $this->getDoctrine()->getManager();
        //$aticles = $em->getRepository('AppBundle:Article')->findLatest();

        $articles = $this->get('article_repository')->findLatest();

        return [
            'articles' => $articles,
        ];
    }

    /**
     * @Route("/articles/{slug}", name="blog_article")
     * @ParamConverter()
     * @Template("blog/article_show.html.twig")
     *
     * See http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function articleShowAction(Article $article)
    {
        return ['article' => $article];
    }
}
