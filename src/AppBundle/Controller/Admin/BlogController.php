<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("%app.admin_url_prefix%/post")
 */
class BlogController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @Route("/", name="admin_post_index")
     * @Method("GET")
     * @Template("admin/blog/index.html.twig")
     */
    public function indexAction()
    {
        //$em = $this->getDoctrine()->getManager();
        //$aticles = $em->getRepository('AppBundle:Article')->findLatest();

        $articles = $this->get('article_repository')->findLatest();

        return [
            'articles' => $articles,
        ];
    }

    /**
     * @Route("/edit/{slug}", name="admin_post_edit")
     * @ParamConverter()
     * @Template("admin/blog/edit.html.twig")
     */
    public function editAction(Request $request, Article $article)
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->add('save', SubmitType::class);

        $handler = $this->get('article_form_handler');

        $handler->handle($form, $request);

        return [
            'article' => $article,
            'form' => $form->createView(),
        ];
    }
}
