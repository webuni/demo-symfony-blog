<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Article;
use AppBundle\Utils\Slugger;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class ArticleHandler
{
    private $entityManager;
    private $slugger;

    public function __construct(EntityManager $entityManager, Slugger $slugger)
    {
        $this->entityManager = $entityManager;
        $this->slugger = $slugger;
    }

    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return false;
        }
        $validArticle = $form->getData();
        $this->createArticle($validArticle);

        return true;
    }

    private function createArticle(Article $article)
    {
        $article->setSlug($this->slugger->slugify($article->getTitle()));
        $this->entityManager->persist($article);

        // pozor na flush
        // pouzijte ho zde, jen kdyz vite, co delate
        // jinak se doporucuje ho volat az v akci kontroleru
        $this->entityManager->flush();
    }
}
