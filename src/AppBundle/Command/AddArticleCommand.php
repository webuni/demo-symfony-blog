<?php

namespace AppBundle\Command;

use AppBundle\Entity\Article;
use AppBundle\Utils\Slugger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class AddArticleCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 3;

    /** @var Slugger */
    protected $slugger;

    /** @var EntityManager */
    protected $em;

    /**
     * @See http://symfony.com/doc/current/components/console/introduction.html
     */
    protected function configure()
    {
        // @See http://symfony.com/doc/current/components/console/introduction.html
        $this
            ->setName('app:article:add')
            ->setDescription('Vytvori a prida clanek do databaze')
            ->addArgument('title', InputArgument::OPTIONAL, 'Nadpis pro clanek')
            ->addArgument('content', InputArgument::OPTIONAL, 'Obsah clanku')
        ;
    }

    /**
     * Tato metoda se provede pred volanim metod interact() a execute(). Jeji hlavni
     * uloha je inicializovat promenne vyuzivane v nasledujicich metodach prikazu.
     *
     * Pozor na to, ze hodnoty v promenne input (options a arguments) jsou validovane az
     * po vykonani metody interact(), takze v teto metode neni mozne slepe verit jejim hodnotam.
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->slugger = $this->getContainer()->get('slugger');
        $this->em = $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Tato metoda se provede po initialize() a pred execute(). Jeji ucel je zkontrolovat, jestli
     * nejaka hodnota z options/arguments nechybi a pokud ano, tak se na ni uzivatele interaktivne dotazat.
     *
     * Tato metoda je volitelna a neni nutne ji implementovat pro vlastni interni prikazy.
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (null !== $input->getArgument('title') && null !== $input->getArgument('content')) {
            return;
        }

        // multi-line messages can be displayed this way...
        $output->writeln('');
        $output->writeln('Add Article Command Interactive Wizard');
        $output->writeln('--------------------------------------');

        // See http://symfony.com/doc/current/components/console/helpers/questionhelper.html
        $console = $this->getHelper('question');

        // Ask for the title if it's not defined
        $title = $input->getArgument('title');
        if (null === $title) {
            $question = new Question(' > <info>Title</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('The title cannot be empty');
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $title = $console->ask($input, $output, $question);
            $input->setArgument('title', $title);
        } else {
            $output->writeln(' > <info>Title</info>: '.$title);
        }

        // Ask for the content if it's not defined
        $content = $input->getArgument('content');
        if (null === $content) {
            $question = new Question(' > <info>Content</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('The content cannot be empty');
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $content = $console->ask($input, $output, $question);
            $input->setArgument('content', $content);
        } else {
            $output->writeln(' > <info>Content</info>: '.$content);
        }
    }

    /**
     * Tato metoda se provede po vykonani interact() a initialize(). Obsahuje podstatu
     * celeho konzoloveho prikazu.
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startTime = microtime(true);

        $title = $input->getArgument('title');
        $content = $input->getArgument('content');

        // create the article
        $article = new Article();
        $article->setTitle($title);
        $article->setSlug($this->slugger->slugify($title));
        $article->setContent($content);
        $article->setSummary($content);
        $article->setPublishedAt(new \DateTime());

        $this->em->persist($article);
        $this->em->flush($article);

        $output->writeln('');
        $output->writeln(sprintf('[OK] Article was successfully created: %s', $article->getTitle()));

        if ($output->isVerbose()) {
            $finishTime = microtime(true);
            $elapsedTime = $finishTime - $startTime;

            $output->writeln(sprintf('[INFO] New article database id: %d / Elapsed time: %.2f ms', $article->getId(), $elapsedTime * 1000));
        }
    }
}
