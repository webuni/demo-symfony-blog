<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    public function __construct()
    {
    }

    public function getGlobals()
    {
        // {{ constant('Namespace\\Classname::CONSTANT_NAME') }}
        // twig v zakladu obsahuje globalni promenou app
        return [];
    }

    public function getNodeVisitors()
    {
        return parent::getNodeVisitors();
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('phpize', [$this, 'phpize'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('camelize', [$this, 'camelize'], ['is_safe' => ['html']]),
        ];
    }

    public function phpize($property)
    {
        return preg_replace('/([a-z]+)([A-Z])/', '\\1_\\2', $property);
    }

    public function camelize($property)
    {
        return preg_replace(['/(_)+(.)/e'], ["strtoupper('\\2')"], $property);
    }

    // the name of the Twig extension must be unique in the application. Consider
    // using 'app.extension' if you only have one Twig extension in your application.
    public function getName()
    {
        return 'app.extension';
    }
}
