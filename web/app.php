<?php

/*
 * This file is part of the Symfony Minimal Edition package.
 *
 * (c) Webuni s.r.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

date_default_timezone_set('Europe/Prague');
umask(0000);

require_once __DIR__.'/../app/autoload.php';

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

$kernel = new AppKernel('prod', false);
$kernel = new AppCache($kernel);

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
